import segmentation
import cv2
import matplotlib.pyplot as plt

(model_disease, model_lung) = segmentation.load(
    "./saved_model/seg-dataset-300.h5", "./saved_model/seg-dataset-lung-300.h5"
)
percent, image = segmentation.predict(
    model_disease, model_lung, "./images/val_frames/tr_im_z050.png"
)
print(percent)
cv2.imwrite("result.png", image)
plt.imshow(image)
plt.show()

# model_disease = segmentation.load('./saved_model/seg-300.h5', './saved_model/model-lung.h5')
# segmentation.predict(model_disease, None, "./images/train_frames/tr_im_z002.png")
