from keras.preprocessing.image import img_to_array
from keras.applications.resnet50 import ResNet50
from keras.layers import Input
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input
from keras.utils import np_utils, normalize

from keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2
import redis
import json
import shutil
import time
import tempfile
import base64
from pathlib import Path


class covid_test_api:
    covid_detected = bool(False)

    def __init__(self, model_path):
        # load the trained convolutional neural network
        self.model = load_model(model_path)
        # ResNet50
        self.network = ResNet50(
            input_tensor=Input(shape=(416, 416, 3)),
            weights="imagenet",
            include_top=False,
        )

    def detect(self, path_image_source, path_image_target):
        imageTemp = image.load_img(path_image_source, target_size=(416, 416))
        x = image.img_to_array(imageTemp)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        features = self.network.predict(x)
        features = np.sum(np.sum(features, axis=1), axis=1)
        # features= features.reshape((1,features.shape[2]))

        features = normalize(features, axis=1, order=2)
        features = features.reshape((1, 1, features.shape[1]))
        score = self.model.predict(features)

        (notCovid, covid) = score[0][0]

        # build the label
        label = "covid" if covid > notCovid else "Not Covid"
        self.covid_detected = bool(True) if covid > notCovid else bool(False)
        proba = covid if covid > notCovid else notCovid
        label = "{}: {:.2f}%".format(label, proba * 100)

        # draw the label on the image
        output = imutils.resize(cv2.imread(path_image_source), width=400)
        cv2.putText(
            output, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2
        )

        # show the output image
        cv2.imwrite(path_image_target, output)

        metadata = {"accuracy": proba * 100, "detect": self.covid_detected}
        result = {"binary": path_image_target, "metadata": metadata}
        return result


if __name__ == "__main__":
    my_redis = redis.Redis(host="localhost", port=6379, db=0)
    pubsub = my_redis.pubsub(ignore_subscribe_messages=True)

    class_ia_api = covid_test_api("resnet50_covid_not_covid.model")

    pubsub.subscribe("covid-classification-server")

    while True:
        time.sleep(0.05)
        message = pubsub.get_message()
        if message:
            tmp_dir = Path(tempfile.mkdtemp(prefix="classification"))
            data = json.loads(message["data"].decode())
            result = {"id": data["id"], "images": []}
            print("received")
            for image in data["images"]:
                img_pk = image["id"]
                infile = tmp_dir / f"in_{img_pk}"
                outfile = tmp_dir / f"out_{img_pk}.png"
                infile.write_bytes(base64.b64decode(image["binary"].encode()))
                ia_result = class_ia_api.detect(str(infile), str(outfile))
                result["images"].append(
                    {
                        "id": img_pk,
                        "binary": base64.b64encode(outfile.read_bytes()).decode(),
                        "metadata": ia_result["metadata"],
                    }
                )
            print("processed")
            my_redis.publish("covid-classification-client", json.dumps(result))
            shutil.rmtree(str(tmp_dir))
