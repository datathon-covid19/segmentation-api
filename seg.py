import segmentation
import cv2
import redis
import json
import shutil
import time
import tempfile
import base64
import os
from pathlib import Path

from resnet50_n_test_network import covid_test_api


# (model_disease, model_lung) = segmentation.load('./saved_model/seg-19-04-300.h5', './saved_model/model-lung.h5')
# percent, image = segmentation.predict(model_disease, model_lung, "./images/val_frames/tr_im_z050.png")

# model_disease = segmentation.load('./saved_model/seg-300.h5', './saved_model/model-lung.h5')
# segmentation.predict(model_disease, None, "./images/train_frames/tr_im_z002.png")


if __name__ == "__main__":
    my_redis = redis.Redis(
        host=os.environ.get("REDIS_HOST", "localhost"), port=6379, db=0
    )
    pubsub = my_redis.pubsub(ignore_subscribe_messages=True)

    (model_disease, model_lung) = segmentation.load(
        "./saved_model/seg-disease.h5", "./saved_model/seg-lung.h5"
    )

    class_ia_api = covid_test_api("./saved_model/resnet50-1N-covid_not_covid.model")

    pubsub.subscribe("covid-server")

    while True:
        time.sleep(0.05)
        message = pubsub.get_message()
        if message:
            tmp_dir = Path(tempfile.mkdtemp(prefix="segmentation"))
            data = json.loads(message["data"].decode())
            result = {"id": data["id"], "images": []}
            for image in data["images"]:
                img_pk = image["id"]
                infile = tmp_dir / f"in_{img_pk}"
                outfile = tmp_dir / f"out_{img_pk}.png"
                infile.write_bytes(base64.b64decode(image["binary"].encode()))
                if data["algotype"] == "segmentation":
                    percent, image = segmentation.predict(
                        model_disease, model_lung, str(infile)
                    )
                    cv2.imwrite(str(outfile), image)
                    result["images"].append(
                        {
                            "id": img_pk,
                            "binary": base64.b64encode(outfile.read_bytes()).decode(),
                            "metadata": {"percentage of diseased area": percent},
                        }
                    )
                elif data["algotype"] == "classification":
                    ia_result = class_ia_api.detect(str(infile), str(outfile))
                    result["images"].append(
                        {
                            "id": img_pk,
                            "binary": base64.b64encode(outfile.read_bytes()).decode(),
                            "metadata": ia_result["metadata"],
                        }
                    )
            my_redis.publish("covid-client", json.dumps(result))
            shutil.rmtree(str(tmp_dir))
