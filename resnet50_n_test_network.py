from keras.preprocessing.image import img_to_array
from keras.applications.resnet50 import ResNet50
from keras.layers import Input
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input
from keras.utils import np_utils, normalize

from keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2

class covid_test_api:
  covid_detected = bool(False)

  def __init__(self, model_path):
    # load the trained convolutional neural network
    self.model = load_model(model_path)
    #ResNet50
    self.network = ResNet50(input_tensor=Input(shape=(416,416,3)),weights='imagenet', include_top=False)

  def detect(self, path_image_source, path_image_target):
    imageTemp = image.load_img(path_image_source, target_size=(416, 416))
    x = image.img_to_array(imageTemp)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = self.network.predict(x)
    features = np.sum(np.sum(features,axis=1),axis=1)
    #features= features.reshape((1,features.shape[2]))

    features = normalize(features, axis=1,order=2)
    features= features.reshape((1,1,features.shape[1]))
    score = self.model.predict(features)

    covid = score[0][0][0]
    
    # build the label
    label = "covid" if covid > 0.5 else "Not Covid"
    proba = covid if covid > 0.5 else 1 - covid
    label = "{}: {:.2f}%".format(label, proba * 100)
    self.covid_detected = bool(True) if covid > 0.5 else bool(False)

    # draw the label on the image
    output = imutils.resize(cv2.imread(path_image_source), width=400)
    cv2.putText(output, label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

    # show the output image
    cv2.imwrite(path_image_target, output)

    metadata = { "accuracy": proba * 100, "detect": self.covid_detected}
    result = { "resultImage": path_image_target, "metadata": metadata }
    return result