import tensorflow as tf
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
import numpy as np
import tensorflow.keras.backend as K
from skimage.transform import resize

IMG_WIDTH = 512
IMG_HEIGHT = 512
IMG_CHANNELS = 3

def create_mask(pred_mask):
    pred_mask = tf.argmax(pred_mask, axis=-1)
    pred_mask = pred_mask[..., tf.newaxis]
    return pred_mask[0]

def dice_coef_disease(y_true, y_pred, smooth=1):
    size_i2 = tf.math.count_nonzero(y_true)
    pred_mask = tf.argmax(y_pred, axis=-1)
    pred_mask = pred_mask[..., tf.newaxis]
    matched = tf.math.count_nonzero(y_true * tf.cast(pred_mask,tf.float32), dtype=tf.float32)
    find_c = tf.math.count_nonzero(pred_mask)
    find_c = tf.cast(find_c, tf.float32)
    find_c_true = tf.math.count_nonzero(y_true)

    try:
        add = find_c + tf.cast(find_c_true,tf.float32)
        dc = 2. * matched / add
    except ZeroDivisionError:
        dc = 1.
    return dc

def dice_coef(y_true, y_pred, smooth=1):
    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
    return (2.0 * intersection + smooth) / (
        K.sum(K.square(y_true), -1) + K.sum(K.square(y_pred), -1) + smooth
    )


def dice_coef_loss(y_true, y_pred):
    return 1 - dice_coef(y_true, y_pred)


def load(path_to_disease, path_to_lung):
    print("[INFO] Loading network...")
    model_disease = tf.keras.models.load_model(
        path_to_disease,
        custom_objects={"dice_coef": dice_coef_disease}
    )
    model_lung = tf.keras.models.load_model(
        path_to_lung,
        custom_objects={"dice_coef_loss": dice_coef_loss, "dice_coef": dice_coef},
    )
    print("[INFO] Network loaded !")
    return (model_disease, model_lung)


def predict_disease(model, path):
    image = cv2.imread(path)
    orig = image.copy()
    image = cv2.resize(image, (IMG_HEIGHT,IMG_WIDTH))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    mask = create_mask(model.predict(image))
    indices = np.where(mask > 0)
    orig[indices[0], indices[1], :] = [0, 0, 255]
    return (np.sum(mask > 0), orig)


def predict_lung(model, path):
    lung_image = cv2.imread(path)
    X_test = np.zeros((1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
    img = np.expand_dims(resize(lung_image, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True), axis=0)
    print(img.shape)
    X_test[0] = img
    preds_test = model.predict(X_test, verbose=1)
    ind = 0
    preds_test_t = (preds_test > 0.5).astype(np.uint8)
    indices = np.where(preds_test_t[ind] > 0)
    X_test[ind][indices[0], indices[1], :] = [0, 0, 255]
    n_pixel = np.sum(preds_test_t[ind] > 0)

    return n_pixel, X_test[ind]


def predict(model_disease, model_lung, path):
    (disease_nbpix_detected, image_disease) = predict_disease(model_disease, path)
    (lung_nbpix_detected, image_lung) = predict_lung(model_lung, path)
    if lung_nbpix_detected > 0:
        percent = 100 * disease_nbpix_detected / lung_nbpix_detected
        percent = str(round(percent, 2)) + '%'
    else:
        percent = None
    return percent, image_disease
