FROM tensorflow/tensorflow:2.1.0-py3

RUN apt update && apt upgrade -y
RUN apt install -y libgtk2.0-dev

RUN python -m pip install --upgrade pip
RUN python -m pip install matplotlib numpy opencv-python redis keras imutils pillow scikit-image

COPY . /srv

WORKDIR /srv

RUN chmod +x entrypoint.sh

ENTRYPOINT ["/srv/entrypoint.sh"]

#docker run -it -v "$(PWD):/srv" tensorflow/tensorflow:2.1.0-py3
